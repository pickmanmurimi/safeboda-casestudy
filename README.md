# SAFEBODA PROMO CODE API
CASE STUDY FOR Software Engineer

## project Structure.
The project follows a modular structure that tries to "esc" from the traditional Laravel
project structure, in favor for a modular structure that promises more scalability as the
project grows.

Instead of having everything in the App folder, we have everything placed in the Modules 
folder, that houses all the modules required for this particular API.

To get an in depth view of the modular structure, kindly refer to nwidart laravel Modules
https://nwidart.com/laravel-modules/v6/introduction. I help maintain the package.

## Project Setup.

This explains how to get the project running on your setup.

### Requirements

- apache/Nginx
- php 8.^
- mysql:8.0
- sqlite

## Credentials
All environment variables can be set via the .env.
simply copy and paste the `.env.example` file as `.env`.

Change the `DB` credentials to match your setup.

### Commands
TR;DR

        php artisan module:enable
        php artisan migrate
        php artisan passport:install
        php artisan module:seed

To get started you will need to run the following commands to set up modules and seed the database.

Let's start by enabling all the modules first.

    php artisan module:enable

We will now create our database tables.
_This assumes you have added the DB credentials as described in the `Credentials`
section._

    php artisan migrate

Next we will call initialize passport

    php artisan passport:install

Dummy data and some defaults will need to be added to the database for us to be able to interact with the
api via api clients like postman.

    php artisan module:seed


### Tests
Tests have been included in the project.
You can run all the tests by simply running the following commands.

    php artisan test

Tests have been placed in the respective module folders.

Since Promo Codes is the main aim for this project, bulk of the tests are found in `Modules/PromoCodes/Tets/Feature`.

If you are on PhpStorm, you can also configure your test suit and run tests from the editors interface.

### Authentication
There are 2 main users of the api, customers and admins.

Though the initial plan was to develop functionality covering all the users, bulk of the functionality will require admin
login. Though some areas would make more sense if a customer or rider is making the requests, time constraints couldn't
allow exploring farther. Thus, you just need to log in as the admin, scopes and guards have been set up to show what is
possible by just extending the current api.

### Authorization
Authorization has been built around permissions. This makes the api more robust and authorization very scalable.
All default Roles and Permissions can be found in the Authentication Module.
`Modules/Authentication/Database/Seeders/PermissionsSeeder`

### Assumptions and omissions

To get the promo codes working, some assumptions have been made around the helper data needed. Some omissions were done to
enable showcase the basic functionality of the promo codes.

- There is no CRUD for Customers and Admin users.
- There is no Events CRUD.
- Authentication only serves login and authorization.
- No payment logic or module has been included.

### Logging & Slack

To keep watch on the api, a slack integration for logging has been added, this means, all log
messages from the api will be logged to slack.

To enable the integration, you simply need to add your slack incoming webhooks
web url on the .env file.

    LOG_SLACK_WEBHOOK_URL=

### APi Docs

Though not very comprehensive or complete in terms of what could have been possible with better descriptions or
documentations like Swagger, A postman collection has been set up to show the apis from a client. \
The api structure only needs a brief introduction to get you going. \
It follows the Rest structure, which means status codes and HTTP verbs serve a big role.

For any api, the url has been broken down into, the `version`, `module` , `user_scope`, `resource`.

For example the create promo code api endpoint, `api/v1/promo-code/admin/promo-code`.

- api/v1 - version
- promo-code - module
- admin - scope
- promo-code - resource.

