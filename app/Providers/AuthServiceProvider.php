<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;
use Modules\PromoCode\Entities\PromoCode;
use Modules\PromoCode\Policies\PromoCodePolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        PromoCode::class => PromoCodePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

//        Passport::routes();
        Passport::tokensCan([
            'admin' => 'Access Admin Backend',
            'customer' => 'Access Customer App',
        ]);

        Passport::setDefaultScope([
            'customer',
        ]);
    }
}
