<?php

namespace Tests;

use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\ClientRepository;
use Laravel\Passport\Passport;
//use Modules\Authentication\Database\Seeders\PermissionsSeeder;
use Modules\AdminUser\Entities\AdminUser;
use Modules\Authentication\Database\Seeders\PermissionsSeeder;
use Modules\Customer\Entities\Customer;
use Modules\User\Entities\User;
use Spatie\Permission\Models\Role;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call("migrate:fresh");

    }

    /**
     * authenticate
     */
    public function authenticateAdmin( $guard = 'admin', $role = 'super-admin' ): void
    {
        /** @var AdminUser $user */
        $user = AdminUser::factory()->create();

        $this->seed( PermissionsSeeder::class );

        $user->assignRole( $role);

        Passport::actingAs($user, ['admin'], $guard);
    }

    /**
     * authenticate
     */
    public function authenticateCustomer( $guard = 'customer', $role = '' ): void
    {
        /** @var Customer $user */
        $user = Customer::factory()->create();

//        $this->seed( PermissionsSeeder::class );

//        $user->assignRole( $role);

        Passport::actingAs($user, ['customer'], $guard);
    }

    /**
     * createAccessToken
     */
    public function createAccessToken(): void
    {
        $clientRepository = new ClientRepository();
        $client = $clientRepository->create(
            null, 'Safeboda Personal Access Client', 'http://localhost', '',true
        );
        DB::table('oauth_personal_access_clients')->insert([
            'client_id' => $client->id,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
//        dd($client->toArray());

    }
}
