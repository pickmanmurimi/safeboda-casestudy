<?php

namespace Modules\PromoCode\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Common\Traits\JsonableOptions;
use Modules\Common\Traits\UsesUuid;
use Modules\Event\Entities\Event;
use Modules\PromoCode\Database\factories\PromoCodeFactory;

/**
 * Modules\PromoCode\Entities\PromoCode
 *
 * @property int $id
 * @property string $uuid
 * @property string $code
 * @property string $price
 * @property \Illuminate\Support\Carbon $active_on
 * @property \Illuminate\Support\Carbon $valid_until
 * @property \Illuminate\Support\Carbon|null $deactivated_on
 * @property int $valid_radius
 * @property int $event_id
 * @property array|null $options
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read Event $event a promo code belongs to one event
 * @property-read \Modules\PromoCode\Entities\Option $options_object
 * @method static \Modules\PromoCode\Database\factories\PromoCodeFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode query()
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereActiveOn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereDeactivatedOn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereValidRadius($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCode whereValidUntil($value)
 * @mixin \Eloquent
 */
class PromoCode extends Model
{
    use HasFactory, UsesUuid, JsonableOptions;

    /**
     * @var string[] $fillable
     */
    protected $fillable = [
        'code',
        'price',
        'active_on',
        'valid_until',
        'deactivated_on',
        'valid_radius',
    ];

    /**
     * @var string[] $casts
     */
    protected $casts = [
        'options' => 'json',
        'active_on' => 'datetime',
        'valid_until' => 'datetime',
        'deactivated_on' => 'datetime',
    ];

    /**
     * @return PromoCodeFactory
     */
    protected static function newFactory(): PromoCodeFactory
    {
        return PromoCodeFactory::new();
    }

    /**
     * |--------------------------------------------------------------------------
     * | Relationships
     * |--------------------------------------------------------------------------
     */

    /**
     * @comment a promo code belongs to one event
     * @return BelongsTo
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

}
