<?php

namespace Modules\PromoCode\Http\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Modules\Event\Entities\Event;
use Modules\PromoCode\Entities\PromoCode;
use Modules\PromoCode\Http\Requests\StorePromoCodeRequest;
use Modules\PromoCode\Http\Requests\TestPromoCodeValidityRequest;
use Modules\PromoCode\Http\Requests\UpdatePromoCodeRequest;
use Modules\PromoCode\Services\GeoService;
use Modules\PromoCode\Transformers\PromoCodeResource;

class PromoCodeController extends Controller
{
    /**
     * Get a list of all promo codes
     * @param Request $request
     * @return AnonymousResourceCollection
     * @throws AuthorizationException
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        $this->authorize('viewAny', PromoCode::class);

        $promo_codes = PromoCode::search('code', $request->input('code'))
            ->search('price', $request->input('price'))
            ->search('radius', $request->input('radius_lower'));

//        active filter
        if ($request->input('status') === 'active') {
            $promo_codes = $promo_codes->whereNull('deactivated_on')
                ->where('valid_until', '>', now())
                ->where('active_on', '<', now());
        }

        $promo_codes = $promo_codes->paginate($request->input('paginate', 10));

        return PromoCodeResource::collection($promo_codes);
    }

    /**
     * Get a promo code
     * @param $promo_code
     * @return PromoCodeResource
     * @throws AuthorizationException
     */
    public function show($promo_code): PromoCodeResource
    {
        $this->authorize('view', PromoCode::class);

        /** @var PromoCode $promo_code */
        $promo_code = PromoCode::findUuid($promo_code);

        return new PromoCodeResource($promo_code);
    }

    /**
     * A promo code is valid if either the destination or origin are within the event radius
     * @param TestPromoCodeValidityRequest $request
     * @return JsonResponse
     */
    public function testValidity(TestPromoCodeValidityRequest $request): JsonResponse
    {
        /** @var PromoCode $promo_code */
        $promo_code = PromoCode::whereCode($request->code)->firstOrFail();
        $event = $promo_code->event;
        // destination coordinates
        $destination = (object)['lat' => $request->destination_lat, 'long' => $request->destination_long];
        // origin coordinates
        $origin = (object)['lat' => $request->origin_lat, 'long' => $request->origin_long];

        /**
         * check if the origin or destination is valid
         * Any should be valid if they are within the radius of an event.
         */
        // destination distance from event
        $destination_distance = GeoService::distance($event->lat, $event->long,$destination->lat,$destination->long,'K');
        // origin distance from event
        $origin_distance = GeoService::distance($event->lat, $event->long,$origin->lat,$origin->long,'K');
        // valid radius in meters
        $valid_radius = $promo_code->valid_radius / 1000;
        // valid distance
        $valid_code = ($origin_distance < $valid_radius || $destination_distance < $valid_radius);

        // if the code is valid, return the promo code details and a polyline using the destination and
        // origin if the promo code
        if ($valid_code ) {
            return $this->sendSuccess([
                'valid' => true,
                'promo_code' => new PromoCodeResource( $promo_code ),
                'polyline' => [$origin, $destination],
                'origin_distance' => $origin_distance,
                'destination_distance' => $destination_distance,
                'valid_radius' => $valid_radius,
            ], 200);
        }

        // the code is not valid thus an error is returned
        return $this->sendError([
            'valid' => false,
        ], 200);
    }

    /**
     * create new promo code.
     * @param StorePromoCodeRequest $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function store(StorePromoCodeRequest $request): JsonResponse
    {
        $this->authorize('create', PromoCode::class);

        try {
            /** @var Event $event */
            $event = Event::findUuid($request->event);

            if ($event->deactivated_on || $event->ends_on->isBefore($request->active_on)) {
                return $this->sendError('Event closed', 422);
            }

            // add promo code
            $event->promoCodes()->create([
                'code' => $request->code ?? Str::random(5),
                'price' => $request->price,
                'active_on' => $request->active_on,
                'valid_until' => $request->valid_until,
                'valid_radius' => $request->valid_radius,
            ]);

            return $this->sendSuccess('Promo code created', 201);

        } catch (Exception $exception) {
            Log::error($exception); // send to slack
            return $this->sendError('Whoops! an error occured, please try again', 500);
        }

    }

    /**
     * update existing promo code.
     * @param UpdatePromoCodeRequest $request
     * @param $promo_code_uuid
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function update(UpdatePromoCodeRequest $request, $promo_code_uuid): JsonResponse
    {
        $this->authorize('update', PromoCode::class);
        /** @var PromoCode $promo_code */
        $promo_code = PromoCode::findUuid($promo_code_uuid);
        try {

            $event = $promo_code->event;

            // add promo code
            $promo_code->update([
                'code' => $request->code,
                'price' => $request->price,
                'active_on' => $request->active_on,
                'valid_until' => $request->valid_until,
                'valid_radius' => $request->valid_radius,
            ]);

            return $this->sendSuccess('Promo code updated', 200);

        } catch (Exception $exception) {
            Log::error($exception); // send to slack
            return $this->sendError('Whoops! an error occured, please try again', 500);
        }

    }

    /**
     * deactivate promo code.
     * @param $promo_code_uuid
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function deactivate($promo_code_uuid): JsonResponse
    {
        $this->authorize('deactivate', PromoCode::class);

        try {
            /** @var PromoCode $promo_code */
            $promo_code = PromoCode::findUuid($promo_code_uuid);
            $promo_code->update([
                'deactivated_on' => now()
            ]);

            return $this->sendSuccess('Promo code deactivated', 200);
        } catch (Exception $exception) {
            Log::error($exception); // send to slack
            return $this->sendError('Whoops! an error occured, please try again', 500);
        }

    }
}
