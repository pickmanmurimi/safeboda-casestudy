<?php

namespace Modules\PromoCode\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @property mixed $origin_lat
 * @property mixed $origin_long
 * @property mixed $destination_lat
 * @property mixed $destination_long
 * @property mixed $code
 */
class TestPromoCodeValidityRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'origin_lat' => ['required'],
            'origin_long' => ['required'],
            'destination_lat' => ['required'],
            'destination_long' => ['required'],
            'code' => ['required', 'exists:promo_codes,code']
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
