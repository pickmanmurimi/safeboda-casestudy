<?php

namespace Modules\PromoCode\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @property mixed $code
 * @property mixed $price
 * @property mixed $active_on
 * @property mixed $valid_until
 * @property mixed $long
 * @property mixed $lat
 * @property mixed $valid_radius
 */
class UpdatePromoCodeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price' => ['required'],
            'code' => ['required', 'max:6',
                Rule::unique('promo_codes','code')->ignore($this->route('uuid'), 'uuid')],
            'active_on' => ['required', 'date', 'before:valid_until'],
            'valid_until' => ['required', 'date','after:active_on'],
            'valid_radius' => ['required'],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
