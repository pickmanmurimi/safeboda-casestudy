<?php

namespace Modules\PromoCode\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed $event
 * @property mixed $code
 * @property mixed $price
 * @property mixed $active_on
 * @property mixed $valid_until
 * @property mixed $long
 * @property mixed $lat
 * @property mixed $valid_radius
 */
class StorePromoCodeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'event' => ['required'],
            'price' => ['required'],
            'code' => ['nullable','sometimes','unique:promo_codes,code', 'max:6'],
            'active_on' => ['required', 'date', 'before:valid_until'],
            'valid_until' => ['required', 'date','after:active_on'],
            'valid_radius' => ['required'],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
