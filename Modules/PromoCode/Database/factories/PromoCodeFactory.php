<?php

namespace Modules\PromoCode\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Modules\PromoCode\Entities\PromoCode;

class PromoCodeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PromoCode::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'code' => Str::random(5),
            'price' => $this->faker->numberBetween(100, 200),
            'active_on' => now()->subDays(1),
            'valid_until' => now()->addDays(2),
            'valid_radius' => $this->faker->numberBetween(1000, 2000),
        ];
    }
}

