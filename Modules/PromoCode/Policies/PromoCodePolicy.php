<?php

namespace Modules\PromoCode\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class PromoCodePolicy
{
    use HandlesAuthorization;

    /**
     * view
     *
     * @param $user
     * @return bool
     */
    public function viewAny($user): bool
    {
        return $user->can('promo-code.viewAny');
    }

    /**
     * show
     *
     * @param $user
     * @return bool
     */
    public function view($user): bool
    {
        return $user->can('promo-code.view');
    }

    /**
     * update
     *
     * @param  $user
     * @return bool
     */
    public function create($user): bool
    {
        return $user->can('promo-code.create');
    }

    /**
     * update
     *
     * @param  $user
     * @return bool
     */
    public function update($user): bool
    {
        return $user->can('promo-code.update');
    }

    /**
     * deactivate
     *
     * @param $user
     * @return bool
     */
    public function deactivate($user): bool
    {
        return $user->can('promo-code.deactivate');
    }

    /**
     * destroy
     *
     * @param $user
     * @return bool
     */
    public function destroy($user): bool
    {
        return $user->can('promo-code.destory');
    }
}
