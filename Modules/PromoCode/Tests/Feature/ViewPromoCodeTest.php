<?php

namespace Modules\PromoCode\Tests\Feature;

use Exception;
use Modules\Event\Entities\Event;
use Modules\PromoCode\Entities\PromoCode;
use Tests\TestCase;

class ViewPromoCodeTest extends TestCase
{

    /**
     * can view all active promo codes
     *
     * @return void
     * @throws Exception
     */
    public function test_can_view_all_active_promo_codes(): void
    {
        $this->authenticateAdmin();

//        create event
        $this->createPromoCode(3);

        $response = $this->getJson('/api/v1/promo-code/admin/promo-code');

        $response->assertStatus(200);
        $promo_codes = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertCount(3, $promo_codes['data']);
    }

    /**
     * can view all active promo codes
     *
     * @return void
     * @throws Exception
     */
    public function test_can_filter_active_promo_codes(): void
    {
        $this->authenticateAdmin();

//        create event
        $this->createPromoCode(3);

        $response = $this->getJson('/api/v1/promo-code/admin/promo-code?status=active');

        $response->assertStatus(200);
        $promo_codes = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $this->assertCount(3, $promo_codes['data']);
    }

    /**
     * can filter inactive promo codes
     *
     * @return void
     * @throws Exception
     */
    public function test_can_filter_inactive_promo_codes(): void
    {
        $this->authenticateAdmin();

//        create event
        $this->createPromoCode(3);
        PromoCode::first()->update([
            'valid_until' => now()->subHour()
        ]);

        $response = $this->getJson('/api/v1/promo-code/admin/promo-code?status=active');

        $response->assertStatus(200);
        $promo_codes = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $this->assertCount(2, $promo_codes['data']);
    }

    /**
     * can filter inactive promo codes
     *
     * @return void
     * @throws Exception
     */
    public function test_can_get_single_promo_code(): void
    {
        $this->authenticateAdmin();

//        create event
        $this->createPromoCode(3);
        $promo_code = PromoCode::first();

        $response = $this->getJson('/api/v1/promo-code/admin/promo-code/' . $promo_code->uuid );

        $response->assertStatus(200);
        $promo_codes = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertSame($promo_code->uuid, $promo_codes['data']['uuid']);
    }

    /**
     * can filter inactive promo codes
     *
     * @return void
     * @throws Exception
     */
    public function test_can_deactivate_promo_code(): void
    {
        $this->authenticateAdmin();

//        create event
        $this->createPromoCode(3);
        $promo_code = PromoCode::first();

        $response = $this->putJson('/api/v1/promo-code/admin/promo-code/' . $promo_code->uuid . '/deactivate' );

        $response->assertStatus(200);
        $this->assertNotNull(PromoCode::first()->deactivated_on);
    }

    /**
     * create promo codes
     * @param int $promo_code_count
     * @return Event
     */
    public function createPromoCode(int $promo_code_count = 1): Event
    {
        //        create event
        /** @var Event $event */
        return Event::factory()->hasPromoCodes($promo_code_count)->create();
    }
}
