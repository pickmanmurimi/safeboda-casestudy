<?php

namespace Modules\PromoCode\Tests\Feature;

use Exception;
use Modules\Event\Entities\Event;
use Modules\PromoCode\Entities\PromoCode;
use Tests\TestCase;

class ValidatePromoCodeTest extends TestCase
{

    /**
     * can validate promo codes
     *
     * @return void
     * @throws Exception
     */
    public function test_can_validate_promo_codes(): void
    {
        $this->authenticateAdmin();

        $this->createPromoCode(null,['valid_radius' => 50000]);
        /** @var PromoCode $promo_code */
        $promo_code = PromoCode::first();
        $data = [
            "origin_lat" => -1.231980318087085,
            "origin_long" => 36.66793169257575,
            "destination_lat" => -1.231980318087085,
            "destination_long" => 36.76793169257575,
            "code" => $promo_code->code,
        ];

        $response = $this->postJson('/api/v1/promo-code/admin/promo-code/test/validity', $data);
        $response->assertJsonStructure([
            'success', 'message' => [
                'valid',
                'origin_distance',
                'destination_distance',
                'valid_radius',
                'promo_code',
                'polyline'
            ]
        ]);
    }

    /**
     * customer can validate promo codes
     *
     * @return void
     * @throws Exception
     */
    public function test_customer_can_validate_promo_codes(): void
    {
        $this->authenticateCustomer();

        $this->createPromoCode(null,['valid_radius' => 50000]);

        /** @var PromoCode $promo_code */
        $promo_code = PromoCode::first();
        $data = [
            "origin_lat" => -1.231980318087085,
            "origin_long" => 36.66793169257575,
            "destination_lat" => -1.231980318087085,
            "destination_long" => 36.76793169257575,
            "code" => $promo_code->code,
        ];

        $response = $this->postJson('/api/v1/promo-code/customer/promo-code/test/validity', $data);
        $response->assertJsonStructure([
            'success', 'message' => [
                'valid',
                'origin_distance',
                'destination_distance',
                'valid_radius',
                'promo_code',
                'polyline'
            ]
        ]);
    }

    /**
     * can view all active promo codes
     *
     * @return void
     * @throws Exception
     */
    public function test_can_show_invalid_promo_codes(): void
    {
        $this->authenticateAdmin();

        $event_coordinates = [
            'lat' => -1,
            'long' => 36,
        ];
        $this->createPromoCode($event_coordinates, ['valid_radius' => 5000], 3);
        /** @var PromoCode $promo_code */
        $promo_code = PromoCode::first();
        $data = [
            "origin_lat" => -2,
            "origin_long" => 37,
            "destination_lat" => -3,
            "destination_long" => 38,
            "code" => $promo_code->code,
        ];

        $response = $this->postJson('/api/v1/promo-code/admin/promo-code/test/validity', $data);
        $response->assertJsonStructure([
            'success', 'message' => [
                'valid',
            ]
        ]);
        $response->assertJson([
            'success' => false, 'message' => [
                'valid' => false,
            ]
        ]);
    }

    /**
     * create promo codes
     * @param null $event
     * @param null $promo_code
     * @param int $promo_code_count
     * @return Event
     */
    public function createPromoCode($event = null, $promo_code = null, int $promo_code_count = 1): Event
    {
        //        create event
        /** @var Event $event */
        return Event::factory()->hasPromoCodes($promo_code_count, $promo_code)->create($event);
    }
}
