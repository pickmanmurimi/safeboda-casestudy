<?php

namespace Modules\PromoCode\Tests\Feature;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Str;
use Modules\Event\Entities\Event;
use Modules\PromoCode\Entities\PromoCode;
use Tests\TestCase;

class UpdatePromoCodeTest extends TestCase
{
    /**
     * can create promo code
     *
     * @return void
     * @throws Exception
     */
    public function test_can_update_promo_code(): void
    {
        $this->authenticateAdmin();

//        create event
        $promo_code = $this->createPromoCode()->promoCodes->first();

        $data = $this->updateData();

        $response = $this->putJson('/api/v1/promo-code/admin/promo-code/' . $promo_code->uuid, $data);
        $updated_promo_code = PromoCode::first();
        $response->assertStatus(200);
        $this->assertSame($data['code'], $updated_promo_code->code);
    }

    /**
     * create promo codes
     * @param int $promo_code_count
     * @return Event
     */
    public function createPromoCode(int $promo_code_count = 1): Event
    {
        //        create event
        /** @var Event $event */
        return Event::factory()->hasPromoCodes($promo_code_count)->create();
    }

    /**
     * updateData
     * @throws Exception
     */
    public function updateData()
    {
        return [
            'code' => Str::random(5), // promo code (optional)
            'price' => random_int(999, 9999), // promo code amount
            'active_on' => Carbon::now()->addDays(1), // starts on
            'valid_until' => Carbon::now()->addDays(5), // ends on
            'valid_radius' => random_int(999, 9999), // radius in meters
        ];
    }

    /**
     * cannot add a new existing promo code
     *
     * @return void
     * @throws Exception
     */
    public function test_cannot_update_with_identical_promo_codes(): void
    {
        $this->authenticateAdmin();

        $this->createPromoCode(2);
        /** @var PromoCode $promo_code */
        $promo_code = PromoCode::find(1);

        /** @var PromoCode $promo_code1 */
        $promo_code1 = PromoCode::find(2);

//        dd($promo_code->code,$promo_code1->code);

        $data = $this->updateData();
        $data['code'] = $promo_code1->code;

        $response = $this->putJson('/api/v1/promo-code/admin/promo-code/' . $promo_code->uuid, $data);

        $response->assertStatus(422);
        $response->assertJsonStructure(['message', 'errors' => ['code']]);
    }

    /**
     * cannot create empty promo codes
     *
     * @return void
     */
    public function test_cannot_update_empty_promo_code(): void
    {
        $this->authenticateAdmin();
        $this->createPromoCode(2);
        /** @var PromoCode $promo_code */
        $promo_code = PromoCode::find(1);
        $response = $response = $this->putJson('/api/v1/promo-code/admin/promo-code/' . $promo_code->uuid, []);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            ['message',
                'errors' => ['code', 'price', 'active_on', 'valid_until', 'valid_radius']
            ]);
    }

    /**
     * cannot create backdated promo codes
     * the valid_until date must be greater than active_on date
     *
     * @return void
     * @throws Exception
     */
    public function test_cannot_update_with_backdated_promo_code_date(): void
    {
        $this->authenticateAdmin();
//        create event
        /** @var Event $event */
        $promo_code = $this->createPromoCode()->promoCodes->first();
//        create promo code

        $data = $this->updateData();

        $data['active_on'] = Carbon::now()->addDays(1); // starts on
        $data['valid_until'] = Carbon::now()->subDays(5); // date before active on date

        $response = $response = $this->putJson('/api/v1/promo-code/admin/promo-code/' . $promo_code->uuid, $data);

        $response->assertStatus(422);
        $response->assertJsonStructure(
            ['message',
                'errors' => ['valid_until']
            ]);

    }

    /**
     * only admins can create promo codes
     *
     * @return void
     * @throws Exception
     */
    public function test_customers_cannot_update_promoCodes(): void
    {
        $this->authenticateCustomer();

        /** @var Event $event */
        $promo_code = $this->createPromoCode()->promoCodes->first();

        $data = $this->updateData();

        $response = $this->putJson('/api/v1/promo-code/admin/promo-code/' . $promo_code->uuid, $data);

        $response->assertStatus(401);
        $response->assertJson(['message' => 'Unauthenticated.']);
    }

    /**
     * only authorized admins can create promo codes
     *
     * @return void
     * @throws Exception
     */
    public function test_only_authorized_admins_can_update_promo_codes(): void
    {
        $this->authenticateAdmin('admin', 'auditor');

        $promo_code = $this->createPromoCode()->promoCodes->first();

        $data = $this->updateData();

        $response = $this->putJson('/api/v1/promo-code/admin/promo-code/' . $promo_code->uuid, $data);

        $response->assertStatus(403);
        $response->assertJsonStructure(['message']);
    }
}
