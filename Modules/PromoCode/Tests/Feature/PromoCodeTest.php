<?php

namespace Modules\PromoCode\Tests\Feature;

use Carbon\Carbon;
use Modules\Event\Entities\Event;
use Modules\PromoCode\Entities\PromoCode;
use Tests\TestCase;

class PromoCodeTest extends TestCase
{
    /**
     * can create promo code
     *
     * @return void
     */
    public function test_can_create_promo_code(): void
    {
        $this->authenticateAdmin();

//        create event
        /** @var Event $event */
        $event = Event::factory()->create();
//        create promo code

        $data = [
            'event' => $event->uuid,
            'code' => 'PRO123', // promo code (optional)
            'price' => 200, // promo code amount
            'active_on' => Carbon::now()->addDays(1), // starts on
            'valid_until' => Carbon::now()->addDays(5), // ends on
            'valid_radius' => 5000, // radius in meters
        ];

        $response = $this->postJson('/api/v1/promo-code/admin/promo-code', $data);

        $response->assertStatus(201);
        $this->assertCount(1, PromoCode::all());
        $this->assertCount(1, $event->promoCodes);
    }

    /**
     * can create promo code with optional code
     *
     * @return void
     */
    public function test_can_create_promo_code_with_optional_code(): void
    {
        $this->authenticateAdmin();
//        create event
        /** @var Event $event */
        $event = Event::factory()->create();
//        create promo code

        $data = [
            'event' => $event->uuid,
            'code' => '', // promo code (optional)
            'price' => 200, // promo code amount
            'active_on' => Carbon::now()->addDays(1), // starts on
            'valid_until' => Carbon::now()->addDays(5), // ends on
            'valid_radius' => 5000, // radius in meters
        ];

        $response = $this->postJson('/api/v1/promo-code/admin/promo-code', $data);

        $response->assertStatus(201);
        $this->assertCount(1, PromoCode::all());
        $this->assertCount(1, $event->promoCodes);
    }

    /**
     * cannot create identical promo codes
     *
     * @return void
     */
    public function test_cannot_create_identical_promo_codes(): void
    {
        $this->authenticateAdmin();
//        create event
        /** @var Event $event */
        $event = Event::factory()->hasPromoCodes(1, ['code' => 'TESTCODE',])->create();
        $data = PromoCode::factory()->definition();
        $data['code'] = 'TESTCODE';
        $data['event'] = $event->uuid;

        $response = $this->postJson('/api/v1/promo-code/admin/promo-code', $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(['message', 'errors' => ['code']]);
    }

    /**
     * cannot create empty promo codes
     *
     * @return void
     */
    public function test_cannot_create_empty_promo_code(): void
    {
        $this->authenticateAdmin();
        $response = $this->postJson('/api/v1/promo-code/admin/promo-code', []);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            ['message',
                'errors' => [ 'event', 'price', 'active_on', 'valid_until', 'valid_radius']
            ]);
    }

    /**
     * cannot create backdated promo codes
     * the valid_until date must be greater than active_on date
     *
     * @return void
     */
    public function test_cannot_create_backdated_promo_code(): void
    {
        $this->authenticateAdmin();
//        create event
        /** @var Event $event */
        $event = Event::factory()->create();
//        create promo code

        $data = [
            'event' => $event->uuid,
            'code' => '', // promo code (optional)
            'price' => 200, // promo code amount
            'active_on' => Carbon::now()->addDays(1), // starts on
            'valid_until' => Carbon::now()->subDays(5), // date before active on date
            'valid_radius' => 5000, // radius in meters
        ];

        $response = $this->postJson('/api/v1/promo-code/admin/promo-code', $data);

        $response->assertStatus(422);
        $response->assertJsonStructure(
            ['message',
                'errors' => ['valid_until']
            ]);

    }

    /**
     * only creates promo codes for active events
     * cannot create promo codes for past events
     *
     * @return void
     */
    public function test_creates_promo_codes_for_active_events(): void
    {
        $this->authenticateAdmin();
//        create event
        /** @var Event $event */
        $event = Event::factory()->create(['ends_on' => now()->subDays(2)]);

        $data = PromoCode::factory()->definition();
        $data['event'] = $event->uuid;

        $response = $this->postJson('/api/v1/promo-code/admin/promo-code', $data);
        $response->assertStatus(422);
        $response->assertJson(['message' => 'Event closed', 'success' => false ]);
    }

    /**
     * only creates promo codes for active events
     * cannot create promo codes for deactivated
     *
     * @return void
     */
    public function test_cannot_create_promo_codes_for_deactivated_events(): void
    {
        $this->authenticateAdmin();
//        create event
        /** @var Event $event */
        $event = Event::factory()->create(['deactivated_on' => now()]);

        $data = PromoCode::factory()->definition();
        $data['event'] = $event->uuid;

        $response = $this->postJson('/api/v1/promo-code/admin/promo-code', $data);
        $response->assertStatus(422);
        $response->assertJson(['message' => 'Event closed', 'success' => false ]);
    }

    /**
     * only admins can create promo codes
     *
     * @return void
     */
    public function test_customers_cannot_create_promoCodes(): void
    {
        $this->authenticateCustomer();

        /** @var Event $event */
        $event = Event::factory()->create(['deactivated_on' => now()]);

        $data = PromoCode::factory()->definition();
        $data['event'] = $event->uuid;
        $response = $this->postJson('/api/v1/promo-code/admin/promo-code', $data);

        $response->assertStatus(401);
        $response->assertJson(['message' => 'Unauthenticated.']);
    }

    /**
     * only authorized admins can create promo codes
     *
     * @return void
     */
    public function test_only_authorized_admins_can_create_promo_codes(): void
    {
        $this->authenticateAdmin('admin','auditor');

        /** @var Event $event */
        $event = Event::factory()->create(['deactivated_on' => now()]);

        $data = PromoCode::factory()->definition();
        $data['event'] = $event->uuid;
        $response = $this->postJson('/api/v1/promo-code/admin/promo-code', $data);

        $response->assertStatus(403);
        $response->assertJsonStructure(['message']);
    }
}
