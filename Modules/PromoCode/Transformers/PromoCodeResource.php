<?php

namespace Modules\PromoCode\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed $created_at
 * @property mixed $uuid
 * @property mixed $code
 * @property mixed $price
 * @property mixed $active_on
 * @property mixed $valid_until
 * @property mixed $deactivated_on
 * @property mixed $valid_radius
 */
class PromoCodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     * @return array
     */
    public function toArray($request): array
    {
        return $this->resource ? [
            "uuid" => $this->uuid,
            "code" => $this->code,
            "price" => $this->price,
            "active_on" => $this->active_on,
            "valid_until" => $this->valid_until,
            "deactivated_on" => $this->deactivated_on,
            "status" => $this->valid_until->isFuture() && $this->active_on->isPast() &&
            ($this->deactivated_on === null) ? 'active' : 'inactive',
            "valid_radius" => $this->valid_radius,
            'created_at' => $this->created_at,
            'created_at_readable' => $this->created_at->format('d M Y H:i:s'),
        ] : [];
    }
}
