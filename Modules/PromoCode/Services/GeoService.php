<?php

namespace Modules\PromoCode\Services;

/**
 * offers apis for distance calculation
 */
class GeoService
{
    /**
     * @param $origin_lat
     * @param $origin_long
     * @param $destination_lat
     * @param $destination_long
     * @param $unit
     * @return float|int
     */
    public static function distance($origin_lat, $origin_long, $destination_lat, $destination_long, $unit)
    {

        /**
         * check if the origin and destination are equal
         */
        if (($origin_lat === $destination_lat) && ($origin_long === $destination_long)) {
            return 0;
        }

        /** @var float $theta */
        $theta = $origin_long - $destination_long;
        /** @var float $dist */
        $dist = sin(deg2rad($origin_lat)) * sin(deg2rad($destination_lat)) +
            cos(deg2rad($origin_lat)) * cos(deg2rad($destination_lat)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        // distance in kilometers
        if ($unit === "K") {
            return ($miles * 1.609344);
        }

        // distance in miles
        return $miles;
    }
}
