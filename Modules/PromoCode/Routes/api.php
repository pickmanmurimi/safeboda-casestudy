<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// customer
Route::group(['prefix' => 'customer', 'middleware' => ['auth:customer', 'scopes:customer']], function () {
// test promo code validity
    Route::post('promo-code/test/validity', 'PromoCodeController@testValidity');
});

// admin
Route::group(['prefix' => 'admin', 'middleware' => ['auth:admin', 'scopes:admin']], function () {
    Route::get('promo-code', 'PromoCodeController@index');
    Route::get('promo-code/{uuid}', 'PromoCodeController@show');
    Route::post('promo-code', 'PromoCodeController@store');
    Route::put('promo-code/{uuid}', 'PromoCodeController@update');
    Route::put('promo-code/{uuid}/deactivate', 'PromoCodeController@deactivate');
    // test promo code validity
    Route::post('promo-code/test/validity', 'PromoCodeController@testValidity');
});
