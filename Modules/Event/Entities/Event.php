<?php

namespace Modules\Event\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Common\Traits\JsonableOptions;
use Modules\Common\Traits\UsesUuid;
use Modules\Event\Database\factories\EventFactory;
use Modules\PromoCode\Entities\PromoCode;

/**
 * Modules\Event\Entities\Event
 *
 * @property int $id
 * @property string $uuid
 * @property string $name
 * @property float $lat
 * @property float $long
 * @property \Illuminate\Support\Carbon $starts_on
 * @property \Illuminate\Support\Carbon $ends_on
 * @property \Illuminate\Support\Carbon|null $deactivated_on
 * @property array|null $options
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Modules\Event\Entities\Option $options_object
 * @property-read \Illuminate\Database\Eloquent\Collection|PromoCode[] $promoCodes an event has many promo codes
 * @property-read int|null $promo_codes_count
 * @method static \Modules\Event\Database\factories\EventFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Event newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Event newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Event query()
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereDeactivatedOn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereEndsOn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereStartsOn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereUuid($value)
 * @mixin \Eloquent
 */
class Event extends Model
{
    use HasFactory, UsesUuid, JsonableOptions;

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'name',
        'lat',
        'long',
        'starts_on',
        'ends_on',
        'deactivated_on',
        'options',
    ];

    /**
     * @var string[] $casts
     */
    protected $casts = [
        'options' => 'json',
        'starts_on' => 'datetime',
        'ends_on' => 'datetime',
        'deactivated_on' => 'datetime',
    ];

    /**
     * @return EventFactory
     */
    protected static function newFactory(): EventFactory
    {
        return EventFactory::new();
    }

    /**
     * |--------------------------------------------------------------------------
     * | Relationships
     * |--------------------------------------------------------------------------
     */

    /**
     * @comment an event has many promo codes
     * @return HasMany
     */
    public function promoCodes(): HasMany
    {
        return $this->hasMany(PromoCode::class);
    }

}
