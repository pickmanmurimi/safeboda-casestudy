<?php
namespace Modules\Event\Database\factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Event\Entities\Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
//        -1.271980318087085, 36.7679525555857
        return [
            'name' => $this->faker->company . ' Event',
            'lat' => $this->faker->latitude(-1.271980318087085,-1.231980318087085),
            'long' => $this->faker->longitude(36.76793169257575, 36.77793169257575),
            'starts_on' => $this->faker->dateTimeBetween('now', '+2 days'),
            'ends_on' => $this->faker->dateTimeBetween('+2 days', '+2 days'),
        ];
    }
}

