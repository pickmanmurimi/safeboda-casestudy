<?php

namespace Modules\Event\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Event\Entities\Event;

class EventDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Event::factory()->count(3)->hasPromoCodes(2)->create();
    }
}
