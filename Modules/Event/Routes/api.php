<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// customer
Route::group(['prefix' => 'admin', 'middleware' => ['auth:admin', 'scopes:admin']], function () {
// test promo code validity
    Route::post('event', 'EventController@index');
});
