<?php

namespace Modules\Event\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed $uuid
 * @property mixed $name
 * @property mixed $lat
 * @property mixed $long
 * @property mixed $starts_on
 * @property mixed $ends_on
 * @property mixed $deactivated_on
 * @property mixed $options
 * @property mixed $created_at
 */
class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource ? [
            "uuid" => $this->uuid,
            "name" => $this->name,
            "lat" => $this->lat,
            "long" => $this->long,
            "starts_on" => $this->starts_on,
            "ends_on" => $this->ends_on,
            "deactivated_on" => $this->deactivated_on,
            "options" => $this->options,
            'created_at' => $this->created_at,
            'created_at_readable' => $this->created_at->format('d M Y H:i:s'),
        ] : [];
    }
}
