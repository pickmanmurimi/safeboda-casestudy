<?php

namespace Modules\Ride\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Common\Traits\JsonableOptions;
use Modules\Common\Traits\UsesUuid;
use Modules\Ride\Database\factories\RideFactory;

/**
 * Modules\Ride\Entities\Ride
 *
 * @property int $id
 * @property string $uuid
 * @property float $pickup_long
 * @property float $pickup_lat
 * @property float $destination_long
 * @property float $destination_lat
 * @property int $customer_id
 * @property int $rider_id
 * @property array|null $options
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Modules\Ride\Entities\Option $options_object
 * @method static \Modules\Ride\Database\factories\RideFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Ride newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ride newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ride query()
 * @method static \Illuminate\Database\Eloquent\Builder|Ride whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ride whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ride whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ride whereDestinationLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ride whereDestinationLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ride whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ride whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ride wherePickupLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ride wherePickupLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ride whereRiderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ride whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ride whereUuid($value)
 * @mixin \Eloquent
 */
class Ride extends Model
{
    use HasFactory, UsesUuid, JsonableOptions;

    /**
     * @var string[] $fillable
     */
    protected $fillable = [
        'pickup_long',
        'pickup_lat',
        'destination_long',
        'destination_lat',
        'options',
    ];

    protected $casts = [
        'options' => 'json'
    ];

    /**
     * @return RideFactory
     */
    protected static function newFactory(): RideFactory
    {
        return RideFactory::new();
    }
}
