<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rides', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->decimal('pickup_long', 10,7);
            $table->decimal('pickup_lat', 10,7);
            $table->decimal('destination_long', 10,7);
            $table->decimal('destination_lat', 10,7);
            $table->integer('customer_id');
            $table->integer('rider_id');

            $table->longText('options')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rides');
    }
}
