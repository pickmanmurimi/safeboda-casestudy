<?php
namespace Modules\Authentication\Services;

use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsSeederService {

    /**
     * assignPermissions
     *
     * @param  mixed $role
     * @param  mixed $actions
     * @param  mixed $scope
     * @return void
     */
    public static function assignPermissions( Role $role, array $actions, string $scope ): void
    {
        try{
            foreach( $actions as $action )
            {
                $role->givePermissionTo( $scope . '.' . $action );
            }
        } catch ( \Spatie\Permission\Exceptions\PermissionAlreadyExists $e )
        {
            Log::error( $e );
        }
    }

    /**
     * createPermissions
     *
     * @param  mixed $actions
     * @param  mixed $scope
     * @param string $guard_name
     * @return void
     */
    public static function createPermissions(array $actions, string $scope , string $guard_name = 'admin'  ): void
    {
        try{
            foreach( $actions as $action )
            {
                Permission::create( [ 'name' => $scope . '.' . $action , 'guard_name' => $guard_name ] );
            }
        } catch ( \Spatie\Permission\Exceptions\PermissionAlreadyExists $e )
        {
            Log::error( $e );
        }
    }

    /**
     * Use this to sim permission via tinker
     *
     * @param Role $role
     * @param string $scope
     * @param string[] $actions
     */
    public static function seedPermission(Role $role, string $scope ,
                                          array $actions  = ['create', 'viewAny', 'view', 'destroy', 'update'] ): void
    {
        $guard_name = $role->guard_name;

        self::createPermissions( $actions, $scope, $guard_name );
        self::assignPermissions( $role, $actions, $scope );
    }
}
