<?php


namespace Modules\Authentication\Database\Seeders;


use Illuminate\Database\Seeder;
use Modules\Authentication\Services\PermissionsSeederService;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionsSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        //PermissionsSeederService
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $actions = ['create', 'viewAny', 'view', 'destroy', 'update'];

        // =================================================================================
        // create permissions
        // =================================================================================
        // admins
        PermissionsSeederService::createPermissions(array_merge($actions, ['deactivate']), 'promo-code', 'admin');

        // customers
        PermissionsSeederService::createPermissions(['viewAny', 'view'], 'promo-code', 'customer');


        // =================================================================================
        // Create Roles
        // =================================================================================
        /** @var Role $super_admin */
        $super_admin = Role::create(['name' => 'super-admin', 'guard_name' => 'admin']);
        /** @var Role $auditor */
        $auditor = Role::create(['name' => 'auditor', 'guard_name' => 'admin']);
        /** @var Role $customer */
        $customer = Role::create(['name' => 'customer', 'guard_name' => 'customer']);

        // =================================================================================
        // assign permissions
        // =================================================================================
        //  supper admin
        PermissionsSeederService::assignPermissions($super_admin, array_merge($actions, ['deactivate']), 'promo-code');

        // admin Users
        PermissionsSeederService::assignPermissions($auditor, ['viewAny', 'view'], 'promo-code');

        // customers
        PermissionsSeederService::assignPermissions($customer, ['viewAny', 'view'], 'promo-code');


    }
}


