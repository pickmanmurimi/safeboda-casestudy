<?php

namespace Modules\Authentication\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Modules\AdminUser\Entities\AdminUser;
use Modules\Authentication\Http\Requests\AdminLoginRequest;
use Modules\Authentication\Http\Requests\CustomerLoginRequest;
use Modules\Customer\Entities\Customer;

class AuthenticationController extends Controller
{
    /**
     * authFail
     * @var array $authFail
     */
    public array $authFail = [
        'message' => 'Wrong Email and Password combination',
        'errors' => [
            'email' => ['Wrong Email and Password combination'],
            'password' => ['Wrong Email and Password combination'],]];

    /**
     * Login in a user in the customer scope
     * userLogin
     */
    public function customerLogin(CustomerLoginRequest $request ): JsonResponse
    {
        /** @var Customer $customer */
        $customer = Customer::where('email', $request->email )->first();

        /**
         * verify customer password
         */
        if( password_verify($request->password, $customer->password) ) {
            // update last time customer logged in
            $customer->update(['last_login' => Carbon::now()]);

            return $this->sendResponse(
                [ 'token' => $customer->createToken('Customer Personal Access Token',['customer'])->accessToken ]);
        }

        return $this->sendResponse($this->authFail,422, false);
    }

    /**
     * Login in a user in the admin scope
     * userLogin
     */
    public function adminLogin(AdminLoginRequest $request ): JsonResponse
    {
        /** @var AdminUser $admin_user */
        $admin_user = AdminUser::where('email', $request->email )->first();

        /**
         * verify customer password
         */
        if( password_verify($request->password, $admin_user->password) ) {
            // update last time customer logged in
            $admin_user->update(['last_login' => Carbon::now()]);

            return $this->sendResponse(
                [ 'token' => $admin_user->createToken('Admin Personal Access Token',['admin'])->accessToken ]);
        }

        return $this->sendResponse($this->authFail,422, false);
    }
}
