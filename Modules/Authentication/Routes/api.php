<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Customer
Route::group(['prefix' => 'customer'], function () {
    Route::post('login', 'AuthenticationController@customerLogin');
});

// Admin
Route::group(['prefix' => 'admin'], function () {
    Route::post('login', 'AuthenticationController@adminLogin');
});
