<?php

namespace Modules\Authentication\Tests\Feature;

use Illuminate\Support\Facades\Artisan;
use Modules\AdminUser\Entities\AdminUser;
use Modules\Customer\Entities\Customer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthenticationTest extends TestCase
{
    /**
     * tests if a customer can log in
     *
     * @return void
     */
    public function test_customer_can_login(): void
    {
        $this->createAccessToken();

        /** @var Customer $customer */
        $customer = Customer::factory()->count(1)->create()->first();
        $data = [
            'email' => $customer->email,
            'password' => '1234567890',
        ];
        $response = $this->postJson('/api/v1/authentication/customer/login', $data);

        $response->assertJsonStructure([
            'token'
        ]);
        $response->assertStatus(200);
    }

    /**
     * tests if an admin can log in
     *
     * @return void
     */
    public function test_admin_can_login(): void
    {
        $this->createAccessToken();
        /** @var AdminUser $admin */
        $admin = AdminUser::factory()->count(1)->create()->first();
        $data = [
            'email' => $admin->email,
            'password' => '1234567890',
        ];
        $response = $this->postJson('/api/v1/authentication/admin/login', $data);

        $response->assertJsonStructure([
            'token'
        ]);
        $response->assertStatus(200);
    }
}
