<?php

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/


use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Modules\AdminUser\Entities\AdminUser;

Artisan::command('create-admin
            { --name=optimus} { --email=optimus@gmail.com}',
    function ($name, $email) {

        if (!AdminUser::whereEmail($email)->first()) {
            try {

                DB::beginTransaction();

                $this->info("creating user $name ");

                // name
                $param['name'] = $name;
                // email
                $param['email'] = $email;

                $validator = Validator::make($param,
                    [
                        'name' => ['required', 'string', 'max:255'],
                        'email' => ['required', 'string', 'email', 'max:255', 'unique:admin_users'],
                    ]);

                if ($validator->fails()) {
                    return $this->info($validator->messages());
                }

                $this->info("Creating user ..... ");

                $password = 1234567890;

                $user = AdminUser::create([
                    'name' => $param['name'],
                    'email' => $param['email'],
                    'password' => Hash::make($password),
                ]);

                $this->info( "Assigning roles ..... ");
                //assign role to user
                $user->assignRole('super-admin');

                // show new account
                $user->OptionsObject()->set('new_account', true);

                DB::commit();

                return $this->info("User created ");
            } catch (Exception $e) {
                $this->info($e->getMessage());
                DB::rollback();
                Log::error($e);
            }
        }

        return $this->info("User Already Exists. ");

    })->describe('Create an User');
